-- 価格差を取得するクエリ

select asks.pair_id as p, asks.exchange_id as ae, bids.exchange_id as be,
  asks.price as ap, bids.price as bp, bids.price - asks.price as diff
from (
  select exchange_id, pair_id, type, min(price) as price
  from quotations
  where type = 1
  group by exchange_id, pair_id, type
) as asks, (
  select exchange_id, pair_id, type, max(price) as price
  from quotations
  where type = 2
  group by exchange_id, pair_id, type
) as bids
where asks.exchange_id != bids.exchange_id
and asks.pair_id = bids.pair_id
and bids.price - asks.price > 0
order by bids.price - asks.price desc
;
