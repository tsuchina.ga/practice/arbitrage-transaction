package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
)

type ExchangeRepository struct {
}

func (rep *ExchangeRepository) FindAll() []model.Exchange {
    db := library.GetConfig().Db

    var exchanges []model.Exchange
    db.Find(&exchanges)

    return exchanges
}

func (rep *ExchangeRepository) FindByName(name string) model.Exchange {
    db := library.GetConfig().Db

    var exchange model.Exchange
    db.Find(&exchange, "name=?", name)

    return exchange
}

func (rep *ExchangeRepository) Create(exchange model.Exchange) model.Exchange {
    db := library.GetConfig().Db

    db.Create(&exchange)

    return rep.FindByName(exchange.Name)
}
