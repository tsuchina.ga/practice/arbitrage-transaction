package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
)

type UserRepository struct {
}

func (rep *UserRepository) FindAll() []model.User {
    db := library.GetConfig().Db

    var users []model.User
    db.Find(&users, "is_valid=?", 1)

    return users
}


