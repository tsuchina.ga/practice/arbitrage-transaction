package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
)

type QuotationRepository struct {
}

func (rep *QuotationRepository) FindAll() []model.Quotation {
    db := library.GetConfig().Db

    var quotations []model.Quotation
    db.Find(&quotations)

    return quotations
}

func (rep *QuotationRepository) FindByExchangeIdAndPairId(exchangeId int, pairId int) []model.Quotation {
    db := library.GetConfig().Db

    var quotations []model.Quotation
    db.Find(&quotations, "exchange_id = ? AND pair_id = ?", exchangeId, pairId)

    return quotations
}

func (rep *QuotationRepository) Create(quotation model.Quotation) model.Quotation {
    db := library.GetConfig().Db

    db.Save(&quotation)

    return quotation
}

func (rep *QuotationRepository) DeleteByExchangeIdAndPairId(exchangeId int, pairId int) {
    db := library.GetConfig().Db

    quotations := rep.FindByExchangeIdAndPairId(exchangeId, pairId)
    db.Delete(&quotations)
}
