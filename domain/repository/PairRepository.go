package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
)

type PairRepository struct {
}

func (rep *PairRepository) FindAll() []model.Pair {
    db := library.GetConfig().Db

    var pairs []model.Pair
    db.Find(&pairs)

    return pairs
}

func (rep *PairRepository) FindByName(name string) model.Pair {
    db := library.GetConfig().Db

    var pair model.Pair
    db.Find(&pair, "name=?", name)

    return pair
}