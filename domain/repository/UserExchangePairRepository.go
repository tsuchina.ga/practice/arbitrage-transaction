package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
    "log"
)

type UserExchangePairRepository struct {
}

func (rep *UserExchangePairRepository) FindAll() []model.UserExchangePair {
    db := library.GetConfig().Db

    var userExchangePairs []model.UserExchangePair
    db.Find(&userExchangePairs, "is_valid=?", 1)

    return userExchangePairs
}

func (rep *UserExchangePairRepository) FindMultiplePairByUserid(userId int) []int {
    db := library.GetConfig().Db

    var pairIds []int

    rows, err := db.Table("user_exchange_pairs").
        Select("pair_id, count(id) as num").
        Where("user_id = ? and is_valid = ? and (is_ask_valid = 1 or is_bid_valid = 1)", userId, 1).
        Group("pair_id").
        Having("count(id) > ?", 1).
        Rows()
    if err != nil {
        log.Println(err)
        return pairIds
    }

    for rows.Next() {
        var pairId int
        var num int
        err = rows.Scan(&pairId, &num)
        if err != nil {
            log.Println(err)
            continue
        }
        pairIds = append(pairIds, pairId)
    }

    return pairIds
}
