package repository

import (
    "tsuchinaga/arbitrage-transaction/domain/model"
    "tsuchinaga/arbitrage-transaction/library"
    "time"
)

type ExchangePairsRepository struct {
}

func (rep *ExchangePairsRepository) FindAll() []model.ExchangePair {
    db := library.GetConfig().Db

    var exchangePairs []model.ExchangePair
    db.Find(&exchangePairs)

    return exchangePairs
}

func (rep *ExchangePairsRepository) FindById(id int) model.ExchangePair {
    db := library.GetConfig().Db

    var exchangePair model.ExchangePair
    db.Find(&exchangePair, "id=?", id)

    return exchangePair
}

func (rep *ExchangePairsRepository) FindByExchangeId(exchangeId int) []model.ExchangePair {
    db := library.GetConfig().Db

    var exchangePairs []model.ExchangePair
    db.Find(&exchangePairs, "exchange_id=?", exchangeId)

    return exchangePairs
}


func (rep *ExchangePairsRepository) FindByExchangeIdAndPairId(exchangeId int, pairId int) model.ExchangePair {
    db := library.GetConfig().Db

    var exchangePair model.ExchangePair
    db.Find(&exchangePair, "exchange_id=? and pair_id=?", exchangeId, pairId)

    return exchangePair
}

func (rep *ExchangePairsRepository) Create(exchangePair model.ExchangePair) model.ExchangePair {
    db := library.GetConfig().Db

    db.Create(&exchangePair)

    return rep.FindByExchangeIdAndPairId(exchangePair.ExchangeId, exchangePair.PairId)
}

func (rep *ExchangePairsRepository) Update(exchangePair model.ExchangePair) model.ExchangePair {
    db := library.GetConfig().Db
    db.Save(&exchangePair)

    return rep.FindById(exchangePair.Id)
}

func (rep *ExchangePairsRepository) InvalidByExchangeId(exchangeId int) {
    db := library.GetConfig().Db
    exchangePairs := rep.FindByExchangeId(exchangeId)

    for _, exchangePair := range exchangePairs {
        exchangePair.IsValid = 0
        exchangePair.UpdatedAt = time.Now()
        db.Save(&exchangePair)
    }
}
