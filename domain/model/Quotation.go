package model

import "time"

type Quotation struct {
    Id int `gorm:"primary_key"`
    ExchangeId int
    PairId int
    Type int
    Price float64
    Unit float64
    IsUsed int `sql:"default:0"`
    CreatedAt time.Time `sql:"default:current_timestamp"`
    UpdatedAt time.Time `sql:"default:current_timestamp"`
}
