package model

import "time"

type ExchangePair struct {
    Id int `gorm:"primary_key"`
    ExchangeId int
    PairId int
    SystemName string
    Description string
    UnitMin float64
    UnitStep float64
    AuxUnitMin float64
    AuxUnitStep float64
    IsToken int `sql:"default:1"`
    IsValid int `sql:"default:1"`
    CreatedAt time.Time `sql:"default:current_timestamp"`
    UpdatedAt time.Time `sql:"default:current_timestamp"`
}
