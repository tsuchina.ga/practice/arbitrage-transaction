package model

import "time"

type UserExchangePair struct {
    Id int `gorm:"primary_key"`
    UserId int
    ExchangeId int
    PairId int
    Min float64 `sql:"default:0"`
    Max float64 `sql:"default:0"`
    IsAskValid int `sql:"default:0"`
    IsBidValid int `sql:"default:0"`
    IsValid int `sql:"default:1"`
    CreatedAt time.Time `sql:"default:current_timestamp"`
    UpdatedAt time.Time `sql:"default:current_timestamp"`
}
