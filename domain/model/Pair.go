package model

import "time"

type Pair struct {
    Id int `gorm:"primary_key"`
    MainCurrencyId int
    SubCurrencyId int
    Name string
    CreatedAt time.Time `sql:"default:current_timestamp"`
    UpdatedAt time.Time `sql:"default:current_timestamp"`
}
