package model

type Exchange struct {
    Id int `gorm:"primary_key"`
    Name string
}
