package model

import "time"

type User struct {
    Id int `gorm:"primary_key"`
    Loginid string
    Password string
    Name string
    Min float32
    IsValid int `sql:"default:1"`
    CreatedAt time.Time `sql:"default:current_timestamp"`
    UpdatedAt time.Time `sql:"default:current_timestamp"`
}
