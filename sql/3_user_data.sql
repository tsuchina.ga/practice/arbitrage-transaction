-- 各種jobのinit後に実行することでadministratorユーザーで仮想的にすべての取引を実行できる

-- ユーザー
TRUNCATE TABLE users;
INSERT INTO users(loginid, password, name) values('administrator', '', 'administrator');

-- ユーザー取引所設定
TRUNCATE TABLE user_exchanges;
INSERT INTO user_exchanges(user_id, exchange_id, api_key, api_secret)
SELECT users.id, exchanges.id, '', '' FROM users, exchanges;

-- ユーザー通貨ペア設定
TRUNCATE TABLE user_exchange_pairs;
INSERT INTO user_exchange_pairs(user_id, exchange_id, pair_id, is_ask_valid, is_bid_valid)
SELECT users.id, exchange_id, pair_id, 1, 1
FROM users, exchange_pairs;
