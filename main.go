package main

import (
    "log"
    "runtime"
    "github.com/carlescere/scheduler"
    "tsuchinaga/arbitrage-transaction/job/user"
    "tsuchinaga/arbitrage-transaction/job/bitflyer"
    "tsuchinaga/arbitrage-transaction/job/zaif"
    "tsuchinaga/arbitrage-transaction/job/bitbank"
)

func main() {
    log.Println("[main] end job")

    // zaif処理
    scheduler.Every(10).Seconds().Run(func(){ zaif.Job() })

    // bitbank処理
    scheduler.Every(10).Seconds().Run(func(){ bitbank.Job() })

    // bitflyer処理
    scheduler.Every(10).Seconds().Run(func(){ bitflyer.Job() })

    log.Println("[main] end job and wait start")

    // user処理
    user.Job()

    runtime.Goexit() // 処理が終わってもプロセスを終了しない
}
