package bitbank

import (
    "log"
    "tsuchinaga/arbitrage-transaction/domain/repository"
    "sync"
    "tsuchinaga/arbitrage-transaction/domain/model"
    "net/http"
    "io/ioutil"
    "encoding/json"
    "strconv"
)

/**
 APIドキュメント: https://docs.bitbank.cc/
 */

func Job() {
    const EXCHANGE = "bitbank"

    log.Println("[" + EXCHANGE + "] start job")

    // 対象のペアを取得
    exchangeRep := repository.ExchangeRepository{}
    exchange := exchangeRep.FindByName(EXCHANGE)

    exchangePairsRep := repository.ExchangePairsRepository{}
    exchangePairs := exchangePairsRep.FindByExchangeId(exchange.Id)

    // 板情報の取得
    var wg sync.WaitGroup

    for _, exchangePair := range exchangePairs {
        wg.Add(1)
        go func(exchangePair model.ExchangePair) {
            defer wg.Done()

            quotationRep := repository.QuotationRepository{}

            currencyPair := exchangePair.SystemName

            quotationRep.DeleteByExchangeIdAndPairId(exchangePair.ExchangeId, exchangePair.PairId)

            res, err := http.Get("https://public.bitbank.cc/" + currencyPair + "/depth")
            if err != nil {
                log.Println(err)
                return
            }

            body, err := ioutil.ReadAll(res.Body)
            if err != nil {
                log.Println(err)
                return
            }

            var bord Bord
            err = json.Unmarshal([]byte(string(body)), &bord)
            if err != nil {
                log.Println(err)
                return
            }

            for i, ask := range bord.Data.Asks {
                if i > 50 {
                    break
                }

                price, _ := strconv.ParseFloat(ask[0], 64)
                unit, _ := strconv.ParseFloat(ask[1], 64)
                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 1,
                    Price: price,
                    Unit: unit,
                })
            }

            for i, bid := range bord.Data.Bids {
                if i > 50 {
                    break
                }

                price, _ := strconv.ParseFloat(bid[0], 64)
                unit, _ := strconv.ParseFloat(bid[1], 64)
                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 2,
                    Price: price,
                    Unit: unit,
                })
            }

            res.Body.Close()
        }(exchangePair)
    }

    log.Println("[" + EXCHANGE + "] end routin call and wait start")
    wg.Wait()

    log.Println("[" + EXCHANGE + "] end job")
}

type Bord struct {
    Data struct{
        Asks [][]string `json:"asks"`
        Bids [][]string `json:"bids"`
    } `json:"data"`
}
