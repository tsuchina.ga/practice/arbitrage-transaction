package bitflyer

import (
    "log"
    "tsuchinaga/arbitrage-transaction/domain/repository"
    "tsuchinaga/arbitrage-transaction/domain/model"
    "time"
)

func init() {
    const EXCHANGE = "bitflyer"

    log.Println("[" + EXCHANGE + "] start init")

    // 取引所がDBに登録されているかをチェック
    rep := repository.ExchangeRepository{}
    exchange := rep.FindByName(EXCHANGE)
    if exchange.Id < 1 {
        log.Println("[" + EXCHANGE + "] record not found and create")
        exchange = rep.Create(model.Exchange{Id: 0, Name: EXCHANGE})
    }

    // 取引所で利用できる通貨ペアをDBに登録・更新
    exchangeId := exchange.Id
    pairs := []Pair{
        {Name: "BTC/JPY", SystemName: "btc_jpy",Description: "BTC/JPY", IsToken: true,
            UnitMin: 1, UnitStep: 1, AuxUnitMin: 0.001, AuxUnitStep: 0.001},
    }

    pairRep := repository.PairRepository{}
    exchangePairRep := repository.ExchangePairsRepository{}

    // 一度exchange_idに紐づくデータを無効化しておく
    exchangePairRep.InvalidByExchangeId(exchangeId)

    for _, pair := range pairs {
        modelPair := pairRep.FindByName(pair.Name)
        exchangePair := exchangePairRep.FindByExchangeIdAndPairId(exchangeId, modelPair.Id)


        if modelPair.Id != 0 {
            isToken := 1
            if pair.IsToken == false {
                isToken = 0
            }

            if exchangePair.Id == 0 {
                exchangePair = exchangePairRep.Create(model.ExchangePair{
                    ExchangeId: exchangeId,
                    PairId: modelPair.Id,
                    SystemName: pair.SystemName,
                    Description: pair.Description,
                    UnitMin: pair.UnitMin,
                    UnitStep: pair.UnitStep,
                    AuxUnitMin: pair.AuxUnitMin,
                    AuxUnitStep: pair.AuxUnitStep,
                    IsToken: isToken,
                })
            } else {
                exchangePair.SystemName = pair.SystemName
                exchangePair.Description = pair.Description
                exchangePair.UnitMin = pair.UnitMin
                exchangePair.UnitStep = pair.UnitStep
                exchangePair.AuxUnitMin = pair.AuxUnitMin
                exchangePair.AuxUnitStep = pair.AuxUnitStep
                exchangePair.IsToken = isToken
                exchangePair.IsValid = 1
                exchangePair.UpdatedAt = time.Now()
                exchangePair = exchangePairRep.Update(exchangePair)
            }
        } else {
            log.Println("[" + EXCHANGE + "] " + pair.SystemName + " はpairにありません")
        }
    }



    log.Println("[" + EXCHANGE + "] end init")
}

type Pair struct {
    Name string
    SystemName string
    Description string
    IsToken bool
    UnitMin float64
    UnitStep float64
    AuxUnitMin float64
    AuxUnitStep float64
}
