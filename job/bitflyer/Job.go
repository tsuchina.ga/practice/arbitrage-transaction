package bitflyer

import (
    "log"
    "tsuchinaga/arbitrage-transaction/domain/repository"
    "sync"
    "tsuchinaga/arbitrage-transaction/domain/model"
    "net/http"
    "io/ioutil"
    "encoding/json"
)

/**
 APIドキュメント: https://lightning.bitflyer.jp/docs
 */

func Job() {
    const EXCHANGE = "bitflyer"

    log.Println("[" + EXCHANGE + "] start job")

    // 対象のペアを取得
    exchangeRep := repository.ExchangeRepository{}
    exchange := exchangeRep.FindByName(EXCHANGE)

    exchangePairsRep := repository.ExchangePairsRepository{}
    exchangePairs := exchangePairsRep.FindByExchangeId(exchange.Id)

    // 板情報の取得
    var wg sync.WaitGroup

    for _, exchangePair := range exchangePairs {
        wg.Add(1)
        go func(exchangePair model.ExchangePair) {
            defer wg.Done()

            quotationRep := repository.QuotationRepository{}

            currencyPair := exchangePair.SystemName

            quotationRep.DeleteByExchangeIdAndPairId(exchangePair.ExchangeId, exchangePair.PairId)

            res, err := http.Get("https://api.bitflyer.jp/v1/board?product_code=" + currencyPair)
            if err != nil {
                log.Println(err)
                return
            }

            body, err := ioutil.ReadAll(res.Body)
            if err != nil {
                log.Println(err)
                return
            }

            var bord Bord
            err = json.Unmarshal([]byte(string(body)), &bord)
            if err != nil {
                log.Println(err)
                return
            }

            for i, ask := range bord.Asks {
                if i > 50 {
                    break
                }
                price := ask.Price
                unit := ask.Size
                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 1,
                    Price: price,
                    Unit: unit,
                })
            }

            for i, bid := range bord.Bids {
                if i > 50 {
                    break
                }

                price := bid.Price
                unit := bid.Size
                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 2,
                    Price: price,
                    Unit: unit,
                })
            }
            res.Body.Close()
        }(exchangePair)
    }

    log.Println("[" + EXCHANGE + "] end routin call and wait start")
    wg.Wait()

    log.Println("[" + EXCHANGE + "] end job")
}

type Bord struct {
    Bids []struct{
        Price float64 `json:"price"`
        Size float64 `json:"size"`
    } `json:"bids"`
    Asks []struct{
        Price float64 `json:"price"`
        Size float64 `json:"size"`
    } `json:"asks"`
}
