package user

import (
    "log"
    "time"
    "tsuchinaga/arbitrage-transaction/domain/repository"
)

func Job() {
    const EXCHANGE = "user"

    log.Println("[" + EXCHANGE + "] start job")

    // userの一覧を取得し、forを回す
    var userRep repository.UserRepository
    users := userRep.FindAll()
    for _, user := range users {
        //log.Println(user)

        //// どの程度の利幅なら手を出すかの設定を取得
        //min := user.Min
        //log.Println(min)

        // userが利用設定している通貨ペアの一覧を取得し、対象の通貨ペアの一覧を作成する
        var userExchangePairRep repository.UserExchangePairRepository
        pairIds := userExchangePairRep.FindMultiplePairByUserid(user.Id)
        for _, pairId := range pairIds {
            log.Println(pairId)
            // 高い買い
            // 安い売り
            // 設定以上の利幅があれば買い注文と売り注文を出す
        }
    }

    log.Println("[" + EXCHANGE + "] end job")

    // 少し休憩して次のサイクルを開始
    time.Sleep(5 * time.Second)
    Job()
}
