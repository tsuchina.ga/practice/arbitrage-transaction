package zaif

import (
    "log"
    "tsuchinaga/arbitrage-transaction/domain/repository"
    "tsuchinaga/arbitrage-transaction/domain/model"
    "net/http"
    "io/ioutil"
    "encoding/json"
    "time"
)

func init() {
    const EXCHANGE = "zaif"

    log.Println("[" + EXCHANGE + "] start init")

    // 取引所がDBに登録されているかをチェック
    rep := repository.ExchangeRepository{}
    exchange := rep.FindByName(EXCHANGE)
    if exchange.Id < 1 {
        log.Println("[" + EXCHANGE + "] record not found and create")
        exchange = rep.Create(model.Exchange{Id: 0, Name: EXCHANGE})
    }

    // 取引所で利用できる通貨ペアをDBに登録・更新
    updatePair(exchange.Id)

    log.Println("[" + EXCHANGE + "] end init")
}

type Pair struct {
    Name string `json:"name"`
    SystemName string `json:"currency_pair"`
    Description string `json:"description"`
    IsToken bool `json:"is_token"`
    UnitMin float64 `json:"item_unit_min"`
    UnitStep float64 `json:"item_unit_step"`
    AuxUnitMin float64 `json:"aux_unit_min"`
    AuxUnitStep float64 `json:"aux_unit_step"`
}

func updatePair(exchangeId int) {
    res, err := http.Get("https://api.zaif.jp/api/1/currency_pairs/all")
    if err != nil {
        log.Println(err)
        return
    }
    defer res.Body.Close()

    body, err := ioutil.ReadAll(res.Body)
    if err != nil {
        log.Println(err)
        return
    }

    var pairs []Pair
    err = json.Unmarshal([]byte(string(body)), &pairs)
    if err != nil {
        log.Println(err)
        return
    }

    pairRep := repository.PairRepository{}
    exchangePairRep := repository.ExchangePairsRepository{}

    // 一度exchange_idに紐づくデータを無効化しておく
    exchangePairRep.InvalidByExchangeId(exchangeId)

    for _, pair := range pairs {
       modelPair := pairRep.FindByName(pair.Name)
       exchangePair := exchangePairRep.FindByExchangeIdAndPairId(exchangeId, modelPair.Id)

       if modelPair.Id != 0 {
           isToken := 1
           if pair.IsToken == false {
               isToken = 0
           }

           if exchangePair.Id == 0 {
               exchangePair = exchangePairRep.Create(model.ExchangePair{
                   ExchangeId: exchangeId,
                   PairId: modelPair.Id,
                   SystemName: pair.SystemName,
                   Description: pair.Description,
                   UnitMin: pair.UnitMin,
                   UnitStep: pair.UnitStep,
                   AuxUnitMin: pair.AuxUnitMin,
                   AuxUnitStep: pair.AuxUnitStep,
                   IsToken: isToken,
               })
           } else {
               exchangePair.SystemName = pair.SystemName
               exchangePair.Description = pair.Description
               exchangePair.UnitMin = pair.UnitMin
               exchangePair.UnitStep = pair.UnitStep
               exchangePair.AuxUnitMin = pair.AuxUnitMin
               exchangePair.AuxUnitStep = pair.AuxUnitStep
               exchangePair.IsToken = isToken
               exchangePair.IsValid = 1
               exchangePair.UpdatedAt = time.Now()
               exchangePair = exchangePairRep.Update(exchangePair)
           }
       } else {
           log.Println("[zaif] " + pair.SystemName + " はpairにありません")
       }
    }
}
