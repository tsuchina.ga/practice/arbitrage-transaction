package zaif

import (
    "log"
    "tsuchinaga/arbitrage-transaction/domain/repository"
    "net/http"
    "io/ioutil"
    "encoding/json"
    "tsuchinaga/arbitrage-transaction/domain/model"
    "sync"
)

/**
 APIドキュメント
 http://techbureau-api-document.readthedocs.io/ja/latest/index.html
 */

func Job() {
    const EXCHANGE = "zaif"

    log.Println("[" + EXCHANGE + "] start job")

    // 対象のペアを取得
    exchangeRep := repository.ExchangeRepository{}
    exchange := exchangeRep.FindByName(EXCHANGE)

    exchangePairsRep := repository.ExchangePairsRepository{}
    exchangePairs := exchangePairsRep.FindByExchangeId(exchange.Id)

    // 板情報の取得
    var wg sync.WaitGroup
    for _, exchangePair := range exchangePairs {
        wg.Add(1)
        go func(exchangePair model.ExchangePair) {
            defer wg.Done()

            quotationRep := repository.QuotationRepository{}

            currencyPair := exchangePair.SystemName

            quotationRep.DeleteByExchangeIdAndPairId(exchangePair.ExchangeId, exchangePair.PairId)

            res, err := http.Get("https://api.zaif.jp/api/1/depth/" + currencyPair)
            if err != nil {
                log.Println(err)
                return
            }

            body, err := ioutil.ReadAll(res.Body)
            if err != nil {
                log.Println(err)
                return
            }

            var bord Bord
            err = json.Unmarshal([]byte(string(body)), &bord)
            if err != nil {
                log.Println(err)
                return
            }

            for i, ask := range bord.Asks {
                if i > 50 {
                    break
                }

                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 1,
                    Price: ask[0],
                    Unit: ask[1],
                })
            }

            for i, bid := range bord.Bids {
                if i > 50 {
                    break
                }

                quotationRep.Create(model.Quotation{
                    ExchangeId: exchangePair.ExchangeId,
                    PairId: exchangePair.PairId,
                    Type: 2,
                    Price: bid[0],
                    Unit: bid[1],
                })
            }

            res.Body.Close()
        }(exchangePair)
    }

    log.Println("[" + EXCHANGE + "] end routin call and wait start")
    wg.Wait()

    log.Println("[" + EXCHANGE + "] end job")
}

type Bord struct {
    Asks [][]float64 `json:"asks"`
    Bids [][]float64 `json:"bids"`
}
