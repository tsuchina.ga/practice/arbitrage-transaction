package library

import (
    "log"
    "sync"
    "os"
    "github.com/BurntSushi/toml"
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

type Config struct {
    Db gorm.DB
    DbDriver string `toml:"DbDriver"`
    DbHost string `toml:"DbHost"`
    DbPort string `toml:"DbPort"`
    DbName string `toml:"DbName"`
    DbUser string `toml:"DbUser"`
    DbPass string `toml:"DbPass"`
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
    return instance
}

func init() {
    once.Do(func() {
        log.Println("[Config] create instance")
        instance = &Config{}
        _, err := toml.DecodeFile("./setting.toml", instance)
        if err != nil {
            log.Println(err)
            os.Exit(-1)
        }

        db, err := gorm.Open(
            instance.DbDriver,
            "host=" + instance.DbHost +
                " port=" + instance.DbPort +
                " dbname=" + instance.DbName +
                " user=" + instance.DbUser +
                " password=" + instance.DbPass +
                " sslmode=disable")

        if err != nil {
            log.Println(err)
            log.Println(instance.DbDriver)
            log.Println(
                "host=" + instance.DbHost +
                " port=" + instance.DbPort +
                " dbname=" + instance.DbName +
                " user=" + instance.DbUser +
                " password=" + instance.DbPass +
                " sslmode=disable")
            os.Exit(-1)
        }
        instance.Db = *db
    })
}
